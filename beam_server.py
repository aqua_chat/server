import os
import subprocess
from threading import Thread
from typing import TextIO

import httpx
from beam import App, Runtime, Image, GpuType, Volume

app = App(
    name='aqua-ollama',
    runtime=Runtime(
        cpu=2,
        memory='4Gi',
        gpu=GpuType.T4,
        image=Image(
            python_packages=['httpx'],
            python_version='python3.10',
            commands=[
                'apt-get update && apt-get install -y curl python3 python3-pip psmisc && curl -L https://ollama.com/download/ollama-linux-amd64 -o /usr/bin/ollama && chmod +x /usr/bin/ollama',
                'killall ollama; env OLLAMA_MODELS=/data/ollama/models ollama serve & sleep 15 && ollama pull mistral-openorca && ollama pull gemma:2b && ollama pull tinyllama; killall ollama'
            ]
        )
    ),
    volumes=[Volume('ollama', '/root/.ollama')]
)

initialized = False


def background_log(pipe: TextIO) -> None:
    for line in pipe:
        print(line)


def boot():
    global initialized

    if initialized:
        return True

    popen = subprocess.Popen(
        ['/usr/bin/ollama', 'serve'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True,
        env={'OLLAMA_MODELS': '/data/ollama/models', **os.environ}
    )

    for pipe_type in (popen.stdout, popen.stderr):
        Thread(target=background_log, args=(pipe_type,)).start()

    for line in popen.stderr:
        if 'CUDA Compute Capability detected' in line:
            initialized = True
            return True


@app.rest_api()
def api(**input):
    boot()

    url = f'http://localhost:11434/{str(input["path"]).removeprefix("/")}'
    params = {k: v for k, v in input.items() if k in ('method', 'params', 'timeout')}
    body = {k: v for k, v in input.items() if k not in ('method', 'params', 'timeout')}

    resp = httpx.request(url=url, json=body, **params)
    return resp.json()
